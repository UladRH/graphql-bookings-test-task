import { IsString } from '@hippo-oss/nest-dto/strict';

export class CreateAccountDto {
  @IsString({ example: 'username', minLength: 3, maxLength: 64 })
  username!: string;

  @IsString({ example: 'password', minLength: 6, maxLength: 64 })
  password!: string;
}
