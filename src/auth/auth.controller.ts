import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Post,
} from '@nestjs/common';

import { AuthService } from './auth.service';
import { WebSessionService } from './web-session.service';
import { CreateAccountDto, LoginDto } from './dto';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('accounts')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiOperation({ description: 'Create a new account' })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'The account has been successfully created',
  })
  async createAccount(@Body() dto: CreateAccountDto): Promise<void> {
    await this.authService.createAccount(dto);
  }

  @Post('session')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiOperation({ description: 'Create a new account' })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'Successfully logged in',
  })
  @ApiResponse({
    status: HttpStatus.UNPROCESSABLE_ENTITY,
    description: 'Invalid credentials',
  })
  async login(@Body() dto: LoginDto) {
    await this.authService.login(dto);
  }

  @Get('session')
  @ApiOperation({ description: 'Get the current session' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully logged in',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Not logged in',
  })
  currentSession() {
    return this.authService.getCurrentAccount();
  }

  @Delete('session')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiOperation({ description: 'Logout' })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'Successfully logged out',
  })
  async logout() {
    await this.authService.logout();
  }
}
