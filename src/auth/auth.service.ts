import {
  Inject,
  Injectable,
  UnauthorizedException,
  UnprocessableEntityException,
} from '@nestjs/common';

import { CreateAccountDto, LoginDto } from './dto';
import { AccountDocument, AccountModel } from './models';
import { AccountRepository } from './account.repository';
import { Argon2, ARGON2 } from '../shared/providers';
import { WebSessionService } from './web-session.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly accountRepository: AccountRepository,
    @Inject(ARGON2) private readonly argon2: Argon2,
    private readonly webSessionService: WebSessionService,
  ) {}

  async createAccount(dto: CreateAccountDto): Promise<void> {
    const hashedPassword = await this.argon2.hash(dto.password);

    await this.accountRepository.create({
      hashedPassword,
      username: dto.username,
    });
  }

  async login(dto: LoginDto): Promise<AccountDocument> {
    const account = await this.findByUsername(dto.username);

    if (!account) {
      throw new UnprocessableEntityException('Invalid username or password');
    }

    const isValid = await this.argon2.verify(
      account.hashedPassword,
      dto.password,
    );

    if (!isValid) {
      throw new UnprocessableEntityException('Invalid username or password');
    }

    this.webSessionService.storeAccount(account);

    return account;
  }

  findByUsername(username: string): Promise<AccountDocument | null> {
    return this.accountRepository.findByUsername(username);
  }

  isLoggedIn(): boolean {
    return this.webSessionService.hasStoredAccount();
  }

  async getCurrentAccount(): Promise<AccountModel> {
    const username = this.webSessionService.getStoredUsername();

    if (!username) {
      throw new UnauthorizedException('Not logged in');
    }

    const account = await this.accountRepository.findByUsername(username);

    if (!account) {
      throw new UnauthorizedException('Not logged in');
    }

    return account;
  }

  async logout(): Promise<void> {
    await this.webSessionService.destroy();
  }
}
