import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { AccountModel } from './models';

export const CurrentAccount = createParamDecorator(
  <K extends keyof AccountModel>(data: K, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const account = request.account;

    return data ? account?.[data] : account;
  },
);
