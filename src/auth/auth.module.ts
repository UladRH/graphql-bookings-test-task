import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { AccountModel, AccountSchema } from './models';
import { AccountRepository } from './account.repository';
import { argon2Provider } from '../shared/providers';
import { WebSessionService } from './web-session.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: AccountModel.name, schema: AccountSchema },
    ]),
  ],
  controllers: [AuthController],
  providers: [
    argon2Provider,

    AuthService,
    AccountRepository,
    WebSessionService,
  ],
})
export class AuthModule {}
