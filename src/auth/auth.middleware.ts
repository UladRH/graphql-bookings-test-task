import { Injectable, NestMiddleware } from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';

import { AuthService } from './auth.service';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  constructor(private readonly authService: AuthService) {}

  async use(req: Request, res: Response, next: NextFunction) {
    (req as any).isAuthenticated = false; // init

    if (this.authService.isLoggedIn()) {
      const account = await this.authService.getCurrentAccount();

      if (account) {
        (req as any).account = account;
        (req as any).isAuthenticated = true;
      } else {
        // session has nonexistent account id
        this.authService.logout();
      }
    }

    next();
  }
}
