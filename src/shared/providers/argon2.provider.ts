import * as argon2 from 'argon2';

import { Provider } from '@nestjs/common/interfaces/modules/provider.interface';

const ARGON2 = 'ARGON2';

type Argon2 = typeof argon2;

const argon2Provider: Provider = {
  provide: ARGON2,
  useValue: argon2,
};

export { ARGON2, Argon2, argon2Provider };
