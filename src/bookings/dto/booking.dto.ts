import { Field, GraphQLISODateTime } from '@nestjs/graphql';

export class BookingDto {
  @Field(() => GraphQLISODateTime)
  fromDate!: Date;

  @Field(() => GraphQLISODateTime)
  toDate!: Date;
}
