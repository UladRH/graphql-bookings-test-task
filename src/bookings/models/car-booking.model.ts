import { Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

import { AccountModel } from '../../auth/models';

export type CarBookingDocument = CarBookingModel & Document;

@Schema({ collection: 'bookings' })
export class CarBookingModel {
  entityType!: string;

  account!: AccountModel;

  fromDate!: Date;

  toDate!: Date;
}

export const CarBookingSchema = SchemaFactory.createForClass(CarBookingModel);
