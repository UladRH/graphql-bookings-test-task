import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Document } from 'mongoose';

import { OfficeModel } from '../../offices/models';
import { CarModel } from '../../cars/models';
import { AccountModel } from '../../auth/models';

export type BookingDocument = BookingModel & Document;

@Schema({ discriminatorKey: 'entityType', collection: 'bookings' })
export class BookingModel {
  @Prop({
    type: String,
    required: true,
    enum: [OfficeModel.name, CarModel.name],
  })
  entityType!: string;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: AccountModel.name })
  account!: AccountModel;

  @Prop({ type: Date, required: true })
  fromDate!: Date;

  @Prop({ type: Date, required: true })
  toDate!: Date;
}

export const BookingSchema = SchemaFactory.createForClass(BookingModel);
