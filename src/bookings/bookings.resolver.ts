import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { NotImplementedException } from '@nestjs/common';

import { BookingsService } from './bookings.service';
import { CreateCarBookingInput, CreateOfficeBookingInput } from './dto';
import { CurrentAccount } from '../auth/current-account.decorator';
import { AccountModel } from '../auth/models';
import { BookingModel } from './models';
import { GetCarsArgs } from '../cars/dto';
import { CarDocument } from '../cars/models';
import { BookingDto } from './dto';

@Resolver(() => BookingModel)
export class BookingsResolver {
  constructor(private readonly bookingsService: BookingsService) {}

  @Query(() => [BookingDto], { name: 'bookings' })
  getAll(@Args() args: GetCarsArgs): Promise<CarDocument[]> {
    throw new NotImplementedException();
  }

  @Mutation(() => BookingModel)
  createOfficeBooking(
    @CurrentAccount() account: AccountModel,
    @Args() args: CreateOfficeBookingInput,
  ) {
    throw new NotImplementedException();
  }

  @Mutation(() => BookingModel)
  createCarBooking(
    @CurrentAccount() account: AccountModel,
    @Args() args: CreateCarBookingInput,
  ) {
    throw new NotImplementedException();
  }
}
