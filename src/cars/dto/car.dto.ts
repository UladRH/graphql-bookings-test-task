import { Field, Float, ID, ObjectType } from '@nestjs/graphql';

import { CarModel } from '../models';

@ObjectType('Office')
export class CarDto implements CarModel {
  @Field(() => ID)
  id!: string;

  @Field()
  model!: string;

  @Field(() => Float)
  pricePerHour!: number;
}
