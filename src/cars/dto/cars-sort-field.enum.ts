import { registerEnumType } from '@nestjs/graphql';

export enum CarsSortField {
  PRICE = 'pricePerHour',
}

registerEnumType(CarsSortField, {
  name: 'CarsSortField',
});
