import { Injectable } from '@nestjs/common';

import { CarsRepository } from './cars.repository';
import { GetCarsArgs } from './dto';
import { CarDocument } from './models';

@Injectable()
export class CarsService {
  constructor(private readonly carsRepository: CarsRepository) {}

  getAll(args: GetCarsArgs): Promise<CarDocument[]> {
    return this.carsRepository.findAll(args);
  }
}
