import { Args, Query, Resolver } from '@nestjs/graphql';

import { CarsService } from './cars.service';
import { OfficeDto } from '../offices/dto';
import { CarDocument } from './models';
import { GetCarsArgs } from './dto';

@Resolver()
export class CarsResolver {
  constructor(private readonly carsService: CarsService) {}

  @Query(() => [OfficeDto], { name: 'cars' })
  getAll(@Args() args: GetCarsArgs): Promise<CarDocument[]> {
    return this.carsService.getAll(args);
  }
}
