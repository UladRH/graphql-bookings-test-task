import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model, QueryOptions } from 'mongoose';

import { CarDocument, CarModel } from './models';
import { GetCarsArgs } from './dto';
import { SortDirection } from '../shared/dto';

type CarsFilterType = Pick<GetCarsArgs, 'model' | 'price'>;
type CarsOptionsType = Pick<
  GetCarsArgs,
  'skip' | 'take' | 'sortBy' | 'sortDirection'
>;

@Injectable()
export class CarsRepository {
  constructor(
    @InjectModel(CarModel.name)
    private readonly CarModel: Model<CarDocument>,
  ) {}

  async findAll(args: GetCarsArgs = new GetCarsArgs()): Promise<CarDocument[]> {
    return this.CarModel.find(
      this.buildFilter(args),
      {},
      this.buildOptions(args),
    ).exec();
  }

  private buildFilter(args: CarsFilterType): FilterQuery<CarDocument> {
    const f = {} as FilterQuery<CarDocument>;

    if (args.model) {
      f.address = { $regex: args.model, $options: 'i' };
    }

    if (args.price) {
      const [gte, lte] = args.price ?? [];
      f.pricePerHour = {};

      if (gte) {
        f.pricePerHour.$gte = gte;
      }

      if (lte) {
        f.pricePerHour.$lte = lte;
      }
    }

    return f;
  }

  private buildOptions(args: CarsOptionsType): QueryOptions {
    const o = {} as QueryOptions;

    if (args.skip) {
      o.skip = args.skip;
    }

    if (args.take) {
      o.limit = args.take;
    }

    if (args.sortBy) {
      const direction = args.sortDirection === SortDirection.ASC ? 1 : -1;

      o.sort = { [args.sortBy]: direction };
    }

    return o;
  }
}
