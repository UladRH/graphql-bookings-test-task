import { ArgsType, Field, Float } from '@nestjs/graphql';

import { PaginationArgs, SortDirection } from '../../shared/dto';
import {
  ArrayMaxSize,
  ArrayMinSize,
  IsEnum,
  IsNumber,
  IsOptional,
  MaxLength,
} from 'class-validator';
import { OfficesSortField } from './offices-sort-field.enum';

@ArgsType()
export class GetOfficesArgs extends PaginationArgs {
  @Field({ nullable: true })
  @MaxLength(100)
  @IsOptional()
  address?: string;

  @Field(() => [Float], { nullable: 'itemsAndList' })
  @IsNumber({}, { each: true })
  @ArrayMinSize(2)
  @ArrayMaxSize(2)
  @IsOptional()
  price?: [number | null, number | null];

  @Field(() => OfficesSortField, { nullable: true })
  @IsEnum(OfficesSortField)
  @IsOptional()
  sortBy?: OfficesSortField;

  @Field(() => SortDirection, { nullable: true })
  @IsEnum(SortDirection)
  @IsOptional()
  sortDirection?: SortDirection;
}
