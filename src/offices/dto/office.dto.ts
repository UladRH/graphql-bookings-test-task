import { Field, Float, ID, ObjectType } from '@nestjs/graphql';

import { OfficeModel } from '../models';

@ObjectType('Office')
export class OfficeDto implements OfficeModel {
  @Field(() => ID)
  id!: string;

  @Field()
  address!: string;

  @Field()
  postalCode!: string;

  @Field(() => Float)
  pricePerHour!: number;
}
