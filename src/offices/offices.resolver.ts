import { Args, Query, Resolver } from '@nestjs/graphql';

import { OfficesService } from './offices.service';
import { OfficeDto } from './dto';
import { GetOfficesArgs } from './dto';
import { OfficeModel } from './models';

@Resolver(() => OfficeDto)
export class OfficesResolver {
  constructor(private readonly officesService: OfficesService) {}

  @Query(() => [OfficeDto], { name: 'offices' })
  getAll(@Args() args: GetOfficesArgs): Promise<OfficeModel[]> {
    return this.officesService.getAll(args);
  }
}
